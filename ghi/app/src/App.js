import React from 'react';
import Nav from './nav.js';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm.js';
import ConferenceForm from './ConferenceForm.js';
import PresentationForm from "./PresentationForm.js";
import AttendConferenceForm from './AttendConferenceForm.js';
import MainPage from "./MainPage.js"
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
function App(props) {
    if (props.attendees === undefined) {
        return null;
    }
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route index element={<MainPage />} />
                    <Route path="locations">
                        <Route path="new" element={<LocationForm />} />
                    </Route>
                    <Route path="conferences">
                        <Route path="new" element={<ConferenceForm />} />
                    </Route>
                    <Route path="attendees">
                        <Route path="new" element={<AttendConferenceForm />} />
                        <Route path="" element={<AttendeesList attendees={props.attendees} />} />
                    </Route>
                    <Route path="presentations">
                        <Route path="new" element={<PresentationForm />} />
                    </Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
